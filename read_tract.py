
import glob

from keras.preprocessing.image import load_img, img_to_array

import matplotlib
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd

import math

from PIL import Image

def read_train_list(fname):

    indata = pd.read_csv(fname)
    return indata.to_numpy()

# read id from csv file
def id_to_image(id_string):
    
    idlist = id_string.split("_")
    # first part is case, remove the letters
    case = idlist[0]
    case = int(case.replace("case", ""))
    # same for day
    day = idlist[1]
    day = int(day.replace("day", ""))
    slicen = int(idlist[3])
    # print(case, day, slicen)
    return case, day, slicen

# get specified image, return file name, height in pixels, width in pixels, pixel height in mm, pixel width in mm
# folder containing images, case number, day number, slice number
def get_image(folder, case, day, slicen):
    
    # pad slice number with zeroes
    slicestr = str(slicen)
    for i in range(0, 4-len(slicestr)):
        slicestr = "0" + slicestr
    iname = "slice_" + slicestr + "_"
        
    # full name based on inputs
    fpart = folder + "/case" + str(case) + "/case" + str(case) + "_day" + str(day) + "/scans/" + iname

    # glob for the 1 matching file, should be list with length 1
    namelist = glob.glob(fpart + "*")
    
    if (len(namelist) > 1):
        raise SystemExit("ERROR: Found more than 1 matching file")

    fullname = ""
    try: 
        fullname = namelist[0]
    except:
        raise SystemExit("ERROR: Did not find matching image file")

    # get other info from file name
    # remove folders
    splitname = fullname.split("/")[-1]
    # on windows backslash means scans remains. Remove if it is there
    splitname = splitname.replace("scans\\", "")    
    # remove .png
    splitname = splitname[:-4]
    splitname = splitname.split("_")
        
    if (len(splitname)) != 6:
        print(splitname)
        raise SystemExit("ERROR: Wrong number of items from filename splitting")
    
    ipix_height = float(splitname[4])
    ipix_width = float(splitname[5])

    # return path name and pixel height/width in mm
    return fullname, ipix_height, ipix_width

# iph and ipw are pixel height and width in mm
def process_image(image, iph, ipw, default_mm, default_pixels):
    # number of pixels to crop in x and y
    pix_cut_y = math.ceil(image.size[1] - default_mm/iph)
    pix_cut_x = math.ceil(image.size[0] - default_mm/ipw)
    # print("Cut pix x =", pix_cut_x, "Cut pix y =", pix_cut_y)
    
    # crop half from each side
    # give co-ordinates in pixels, top left is (0,0), bottom right is (pix_cut_x, pix_cut_y)
    # co-ords are: (left, top, right, bottom)
    im_crop = image.crop(( math.floor(pix_cut_x/2), math.floor(pix_cut_y/2), math.ceil(image.size[0] - (pix_cut_x/2)), math.ceil(image.size[1] - (pix_cut_y/2)) ))

    # print("Crop down to size =",im_crop.size)
    im_resized = im_crop.resize( (default_pixels,default_pixels) )
    
    return im_resized

# just img to array and scale to 0->1 range
def get_array(image):

    img_arr = img_to_array(image)
    img_arr = img_arr.astype('float64')
    img_arr /= 255.0

    return img_arr

def rev_process_image(image, orig_pix_x, orig_pix_y, iph, ipw, default_mm):
    
    # find how many pixels were cut
    pix_cut_y = math.ceil(orig_pix_y - default_mm/iph)
    pix_cut_x = math.ceil(orig_pix_x - default_mm/ipw)
    # print("REV1", pix_cut_x, pix_cut_y)

    # resize to size after cropping
    image = image.resize( (orig_pix_x-pix_cut_x, orig_pix_y-pix_cut_y) )
    # print ("REV2", image.size)
 
    # then "un-crop" back up to original size
    # make empty background of correct size
    im_pad = Image.new(image.mode, (orig_pix_x, orig_pix_y), 0)
    # then add in the resized image
    im_pad.paste(image, (math.floor(pix_cut_x/2), math.floor(pix_cut_y/2)))    
    # print("REV3", im_pad.size)

    return im_pad

# after converting to image values get changed slightly
def digi_mask(mask_arr):
    mask_arr[mask_arr<0.5] = 0.
    mask_arr[mask_arr>0.5] = 1.
    return mask_arr
    
# for masks
def RLEtoPixels(RLE, n_pix_x, n_pix_y):
        
    # if RLE is nan just return array of zeros?
    pix_arr = np.zeros(n_pix_x*n_pix_y)

    if RLE != RLE:
        # print("No mask")
        return pix_arr
    
    # RLE is just a string of numbers, need to format it
    # split by spaces
    RLE = RLE.split(" ")
    # convert to int
    RLE = [int(ii) for ii in RLE]

    # split into start indices and lengths
    pstarts = [RLE[ii] for ii in range(0,len(RLE)) if (ii%2==0)]
    pruns = [RLE[ii] for ii in range(0,len(RLE)) if (ii%2!=0)]
    # print (pstarts)
    # print (pruns)
    
    # Go through RLE
    for start, run in zip(pstarts,pruns):
        # Fill array with 1 if mask is there
        # print (start,run)
        for ii in range(start,start+run):
            # print(ii)
            pix_arr[ii] = 1

    # Then reshape into 2D array, will have same size as image
    # print("before", mask_arr.shape)
    pix_arr = pix_arr.reshape((n_pix_x, n_pix_y))
    # print("after", mask_arr.shape)
    
    return pix_arr

# input is 2d array of mask, digitised back to 0 and 1s
def MasktoRLE(digi_mask_arr):

    # reshape to 1d array
    digi_mask_arr = digi_mask_arr.reshape((digi_mask_arr.shape[0]*digi_mask_arr.shape[1]))
    # print("Flatshape", digi_mask_arr.shape)
    outmask = ""
    ii = 0
    while ii < digi_mask_arr.shape[0]:
        countone = 0
        startind = 0
        # if find a 1
        if (digi_mask_arr[ii] == 1):
            startind = ii
            # find how many subsequent 1s
            while (digi_mask_arr[ii] == 1):
                countone+=1
                ii+=1
            # then write to string
            outmask+=str(startind) + " " + str(countone) + " "

        ii+=1

    # remove trailing space
    outmask=outmask[:-1]
    return outmask
                    
def Plot_Slice_and_Mask(im_slice, im_mask, fignum):

    #if im_slice.size == im_mask.size:
    #    print("Plot: Passed size comparison")
    plt.figure(fignum)
    plt.imshow(im_slice)
    plt.imshow(im_mask, cmap='jet', alpha=0.3)

# stack 2d images or masks
def Stack3D():
    return 1

    
# Go back to 2D slices
def Unstack():
    return 1


def main():
    
    folder = "C:/Users/Adam/Documents/Projects/data_tract_seg/train"
    train_list = "C:/Users/Adam/Documents/Projects/data_tract_seg/train.csv/train.csv"

    # target image size
    default_size_mm = 399.
    default_size_pix = 280
    
    inarray = pd.read_csv(train_list).to_numpy()
    masks = inarray[:,-1]
    inarray = np.delete(inarray, [-1], axis=1)
    
    # 60602 is case 22 day 0 slice 73, stomach
    # 83152 is case 34 day 0 slice 53, stomach
    train_list_entries = [60602, 83152]
    
    numplots = 1
    for ent in train_list_entries:
        # 0 is the ID
        print("\n\n")
        print(inarray[ent][0])
        # get image from ID
        case, day, slicen = id_to_image(inarray[ent][0])
        fname, iph, ipw = get_image(folder, case, day, slicen)
     
        # load and print some info
        #img = load_img(fname, color_mode='grayscale')
        img = Image.open(fname)
        print("Image:",  ent)
        print(type(img))
        print(img.format)
        print(img.mode)
        print(img.size)

        orig_pix_x = img.size[0]
        orig_pix_y = img.size[1]

        # Convert RLE compressed pixels into full pixels
        mask_arr = RLEtoPixels(masks[ent], img.size[0], img.size[1] )
        # array is 1 or 0 for mask

        # original images
        img_arr = get_array(img)

        print("Original image min", np.amin(img_arr), "max", np.amax(img_arr))
        Plot_Slice_and_Mask(img_arr, mask_arr, numplots)
        numplots+=1

        # convert mask array to image so I can use the same functions
        print("Original mask min", np.amin(mask_arr), "max", np.amax(mask_arr))
        maskimage = Image.fromarray(mask_arr)

        # crop and resize both with same functions
        # both process_image and reverse input/output images NOT arrays
        img_p = process_image(img, iph, ipw, default_size_mm, default_size_pix)
        mask_p = process_image(maskimage, iph, ipw, default_size_mm, default_size_pix)    
        print("New image size", img_p.size)

        # processed images
        img_arr_p = get_array(img_p)
        print("Processed image min", np.amin(img_arr_p), "max", np.amax(img_arr_p))
        mask_arr_p = img_to_array(mask_p)
        # go back to 0 and 1s
        mask_arr_p = digi_mask(mask_arr_p)
        print("Processed mask min", np.amin(mask_arr_p), "max", np.amax(mask_arr_p))
        Plot_Slice_and_Mask(img_arr_p, mask_arr_p, numplots)
        numplots+=1
        
        # undo processing to check that original image is retreived 
        img_rev = rev_process_image(img_p, orig_pix_x, orig_pix_y, iph, ipw, default_size_mm)
        mask_rev = rev_process_image(mask_p, orig_pix_x, orig_pix_y, iph, ipw, default_size_mm)
        
        img_arr_rev = get_array(img_rev)
        print("Reversed image min", np.amin(img_arr_rev), "max", np.amax(img_arr_rev))
        # for mask don't want to rescale, just use 0 and 1
        mask_arr_rev = img_to_array(mask_rev)
        print("Reversed mask min", np.amin(mask_arr_rev), "max", np.amax(mask_arr_rev))
        
        #if not (np.array_equiv(img_arr_rev, img_arr)):
        #    print ("Image not matched")
        #if not (np.array_equiv(mask_arr_rev, mask_arr)):
        #    print ("Mask not matched")
        Plot_Slice_and_Mask(img_arr_rev, mask_arr_rev, numplots)
        numplots+=1

        # try reversing mask
        digi_mask_rev = digi_mask(mask_arr_rev)
        outmask = MasktoRLE(digi_mask_rev)
        print ("")
        print ("Original mask:",masks[ent])
        print ("Returned mask:",outmask)
        if (masks[ent] == outmask):
            print ("Got exactly the same mask back!")
        
    # will now show all of the plots
    plt.show()
    exit()
    
if __name__ == "__main__":
    main()
 
