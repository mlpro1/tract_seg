
import tensorflow as tf
from tensorflow import keras
import numpy as np

from types import MethodType


# train model on 3D tract arrays and masks

#import os
#os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
#os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

def build_model(width=280, height=280, depth=144):
    """Build a 3D convolutional neural network model."""

    img_input = keras.layers.Input(shape=(depth, height, width, 1 ))
    
    conv1 = keras.layers.Conv3D(32, (3, 3, 3), activation='relu', padding='same')(img_input)
    conv1 = keras.layers.Dropout(0.2)(conv1)
    conv1 = keras.layers.Conv3D(32, (3, 3, 3), activation='relu', padding='same')(conv1)
    pool1 = keras.layers.MaxPooling3D((2, 2, 2))(conv1)
    
    
    conv2 = keras.layers.Conv3D(64, (3, 3, 3), activation='relu', padding='same')(pool1)
    conv2 = keras.layers.Dropout(0.2)(conv2)
    conv2 = keras.layers.Conv3D(64, (3, 3, 3), activation='relu', padding='same')(conv2)
    pool2 = keras.layers.MaxPooling3D((2, 2, 2))(conv2)
    
    conv3 = keras.layers.Conv3D(128, (3, 3, 3), activation='relu', padding='same')(pool2)
    conv3 = keras.layers.Dropout(0.2)(conv3)
    conv3 = keras.layers.Conv3D(128, (3, 3, 3), activation='relu', padding='same')(conv3)

    up1 = keras.layers.concatenate([keras.layers.UpSampling3D((2, 2, 2))(conv3), conv2], axis=-1)
    conv4 = keras.layers.Conv3D(64, (3, 3, 3), activation='relu', padding='same')(up1)
    conv4 = keras.layers.Dropout(0.2)(conv4)
    conv4 = keras.layers.Conv3D(64, (3, 3, 3), activation='relu', padding='same')(conv4)

    up2 = keras.layers.concatenate([keras.layers.UpSampling3D((2, 2, 2))(conv4), conv1], axis=-1)
    conv5 = keras.layers.Conv3D(32, (3, 3, 3), activation='relu', padding='same')(up2)
    conv5 = keras.layers.Dropout(0.2)(conv5)
    conv5 = keras.layers.Conv3D(32, (3, 3, 3), activation='relu', padding='same')(conv5)
    
    out = keras.layers.Conv3D( 1, (1, 1, 1) , padding='same')(conv5)

    model = keras.Model(img_input,out)
    return model

# shuffle multiple arrays together
def shuffle_in_unison(a, b, c):
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)
    np.random.set_state(rng_state)
    np.random.shuffle(c)

#def write_truth_csv():
    
    # have array, possibly truth labels, predicted, 
   

def main():
    
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
                logical_gpus = tf.config.list_logical_devices('GPU')
                print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
    
    small_p = np.load("small_bowel_array.npy", allow_pickle=True)
    small_masks = np.load("small_bowel_masks.npy", allow_pickle=True)
    small_ids = np.load("small_bowel_ids.npy", allow_pickle=True)
    
    # print (small_ids)
    
    # shuffle in unison, split out part of it for validation
    shuffle_in_unison(small_p, small_masks, small_ids)
    
    print (small_p.shape)
    print (small_masks.shape)
    # print (small_ids)

    model = build_model()
    model.summary()
    
    model.compile(optimizer="rmsprop", loss="sparse_categorical_crossentropy")
    model_history = model.fit(small_p, small_masks, batch_size=1, epochs=10)
        
    
if __name__ == "__main__":
    main()
