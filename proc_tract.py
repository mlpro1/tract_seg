 
from read_tract import read_train_list, id_to_image, get_image, process_image, get_array, rev_process_image, digi_mask, RLEtoPixels, MasktoRLE, Stack3D, Unstack

import glob

from keras.preprocessing.image import load_img, img_to_array

import matplotlib
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd

import math

from PIL import Image

# Go through input CSV and get images, combine into 3D array for each scan
# Does transformations using functions from from read_tract
def process_scans(inarray, inmasks, folder, default_size_mm, default_size_pix):
        
    all_images = []
    all_masks = []
    all_ids = []
    current_images = []
    current_masks = []
    current_ids = []
    
    # for tracking when we have a new scan
    isfirst = 1
    prevcase = -1
    prevday = -1
    counter = 0
        
    # loop through entries in CSV
    for ii in range(0, len(inarray)):
        
        # get image from ID
        case, day, slicen = id_to_image(inarray[ii][0])

        # got to end of a scan, write to final array
        if (case != prevcase or day != prevday) and (isfirst !=1):
            all_images.append(current_images)
            all_masks.append(current_masks)
            all_ids.append(current_ids)
            current_images = []
            current_masks = []
            current_ids = []
            print ("RESET", counter)
            # if (counter == 49):
            #    break
            counter+=1
            # print( np.asarray(all_images).shape )
            # print( np.asarray(all_masks).shape )
            # print( np.asarray(all_ids).shape )

        # print (case,day,slicen)
        # load the image
        fname, iph, ipw = get_image(folder, case, day, slicen)
        img = Image.open(fname)

        orig_pix_x = img.size[0]
        orig_pix_y = img.size[1]

        # Convert RLE compressed pixels into full pixels
        mask_arr = RLEtoPixels(inmasks[ii], img.size[0], img.size[1] )

        # original images
        img_arr = get_array(img)
        
        # convert mask array to image so I can use the same functions
        maskimage = Image.fromarray(mask_arr)
        
        # crop and resize both with same functions
        # both process_image and reverse input/output images NOT arrays
        img_p = process_image(img, iph, ipw, default_size_mm, default_size_pix)
        mask_p = process_image(maskimage, iph, ipw, default_size_mm, default_size_pix)  

        # now have processed image and mask
        img_arr_p = get_array(img_p)
        mask_arr_p = img_to_array(mask_p)
        mask_arr_p = digi_mask(mask_arr_p)
        
        # add them to current entry
        current_images.append(img_arr_p)
        current_masks.append(mask_arr_p)
        current_ids.append(inarray[ii][0])
        
        prevcase = case
        prevday = day
        isfirst = 0
    
    r_im = np.asarray(all_images)
    r_mask = np.asarray(all_masks)
    r_id = np.asarray(all_ids)
    print( r_im.shape )
    print( r_mask.shape )
    print( r_id.shape )    
    
    return r_im, r_mask, r_id

def main():

    # target image size
    default_size_mm = 399.
    default_size_pix = 280
    
    folder = "C:/Users/Adam/Documents/Projects/data_tract_seg/train"
    train_list = "C:/Users/Adam/Documents/Projects/data_tract_seg/train.csv/train.csv"

    inarray = pd.read_csv(train_list).to_numpy()

    # Split input csv into small bowel/large bowel/stomach
    sel_small = (inarray[:,1] == "small_bowel")
    inarray_small = inarray[sel_small,:]
    masks_small = inarray_small[:,-1]
    inarray_small = np.delete(inarray_small, [-1], axis=1)
    inarray_small = np.delete(inarray_small, [-1], axis=1)

    sel_large = (inarray[:,1] == "large_bowel")
    inarray_large = inarray[sel_large,:]
    masks_large = inarray_large[:,-1]
    inarray_large = np.delete(inarray_large, [-1], axis=1)
    inarray_large = np.delete(inarray_large, [-1], axis=1)
    
    sel_st = (inarray[:,1] == "stomach")
    inarray_stomach = inarray[sel_st,:]
    masks_stomach = inarray_stomach[:,-1]
    inarray_stomach = np.delete(inarray_stomach, [-1], axis=1)
    inarray_stomach = np.delete(inarray_stomach, [-1], axis=1)

    # merge each case + day into one 3d image
    small_p, small_masks, small_ids = process_scans(inarray_small, masks_small, folder, default_size_mm, default_size_pix)
    print (small_p.shape)
    print (small_masks.shape)
    print (small_ids.shape)
    np.save("small_bowel_array.npy", small_p)
    np.save("small_bowel_masks.npy", small_masks)
    np.save("small_bowel_ids.npy", small_ids)
    
    large_p, large_masks, large_ids = process_scans(inarray_large, masks_large, folder, default_size_mm, default_size_pix)
    print (large_p.shape)
    print (large_masks.shape)
    np.save("large_bowel_array.npy", large_p)
    np.save("large_bowel_masks.npy", large_masks)
    np.save("large_bowel_ids.npy", large_ids)
    
    stomach_p, stomach_masks, stomach_ids = process_scans(inarray_stomach, masks_stomach, folder, default_size_mm, default_size_pix)
    print (stomach_p.shape)
    print (stomach_masks.shape)
    np.save("stomach_array.npy", stomach_p)
    np.save("stomach_masks.npy", stomach_masks)
    np.save("stomach_ids.npy", stomach_ids)
    
    # train on that array

if __name__ == "__main__":
    main()
